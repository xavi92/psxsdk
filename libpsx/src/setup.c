#include <psx.h>
#include <stdio.h>
#include <stddef.h>
#include <string.h>

extern int __bss_start[];
extern int __bss_end[];

extern void *__ctor_list;
extern void *__ctor_end;


// Function to call static constructors (for C++, etc.)
static void call_ctors(void)
{
	dprintf("Calling static constructors...\n");

	void **p = &__ctor_end - 1;
	
	for(--p; *p != NULL && (int)*p != -1 && p > &__ctor_list; p--)
	{
		dprintf("Constructor address = %x\n", (unsigned int)*p);
		(*(void (**)())p)();
	}
	
	dprintf("Finished calling static constructors\n");
}

void psxsdk_setup()
{


	printf("Initializing PSXSDK... \n");

	dprintf("Clearing BSS space...\n");

// Clear BSS space
	memset(__bss_start, 0, (ptrdiff_t)__bss_end - (ptrdiff_t)__bss_start);

	dprintf("BSS space cleared.\n");

// Call static constructors
	call_ctors();

}
